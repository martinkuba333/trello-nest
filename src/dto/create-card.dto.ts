import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateCardDto {
    @IsString()
    @IsOptional()
    title: string;

    @IsString()
    @IsOptional()
    description: string;
}

export class UpdateCardDto extends CreateCardDto {
    @IsNumber()
    @IsNotEmpty()
    idBoard: number;

    @IsNumber()
    @IsNotEmpty()
    idColumn: number;

    @IsNumber()
    @IsNotEmpty()
    idCard: number;

    @IsNumber()
    @IsNotEmpty()
    cardOrder: number;

    @IsNumber()
    @IsNotEmpty()
    newIdColumn: number;
}
