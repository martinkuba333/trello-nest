import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateColumnDto {
    @IsString()
    @IsOptional()
    title: string;
}

export class UpdateColumnDto extends CreateColumnDto {
    @IsNumber()
    @IsNotEmpty()
    idBoard: number;

    @IsNumber()
    @IsNotEmpty()
    idColumn: number;
}
