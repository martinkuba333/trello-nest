export enum EnumInfoStatusesDto {
    DELETED_BOARD = `Board successfully deleted`,
    DELETED_BOARD_FAIL = 'Board deletion failed',
    DELETED_COLUMN = 'Column successfully deleted',
    DELETED_COLUMN_FAIL = 'Column deletion failed',
    DELETED_CARD = 'Card successfully deleted',
    DELETED_CARD_FAIL = 'Card deletion failed',
}
