export enum ErrorsDto {
    WRONG_BOARD = 'Wrong board',
    WRONG_COLUMN = 'Wrong column',
    CARD_NOT_FOUND = 'Card not found',
    BOARD_NOT_FOUND = 'Board not found',
    COLUMN_NOT_FOUND = 'Column not found',
}
