import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateBoardDto {
    @IsString()
    @IsOptional()
    title: string;
}

export class UpdateBoardDto extends CreateBoardDto {
    @IsNumber()
    @IsNotEmpty()
    idBoard: number;
}
