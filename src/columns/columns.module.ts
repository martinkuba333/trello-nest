import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ColumnsEntity } from './entities/columns.entity';
import { ColumnsService } from './columns.service';
import { ColumnsController } from './columns.controller';
import { BoardEntity } from '../boards/entities/boards.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ColumnsEntity, BoardEntity])],
    providers: [ColumnsService],
    controllers: [ColumnsController],
})
export class ColumnsModule {}
