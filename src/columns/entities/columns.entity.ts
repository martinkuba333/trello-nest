import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { BoardEntity } from '../../boards/entities/boards.entity';

@Entity('columns')
export class ColumnsEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 255 })
    title: string;

    @Column({ type: 'bigint' })
    tsCreation: number;

    @ManyToOne(() => BoardEntity, boardEntity => boardEntity.id)
    board: BoardEntity;
}
