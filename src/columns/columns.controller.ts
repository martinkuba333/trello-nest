import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
} from '@nestjs/common';
import { EnumInfoStatusesDto } from '../dto/enum-info-statuses.dto';
import { ColumnsService } from './columns.service';
import { ColumnsEntity } from './entities/columns.entity';
import { CreateColumnDto, UpdateColumnDto } from '../dto/create-column.dto';

//todo guard if board exists
@Controller('boards/:idBoard/column')
export class ColumnsController {
    constructor(private readonly columnsService: ColumnsService) {}

    @Get('get')
    getColumns(@Param('idBoard') idBoard: number): Promise<ColumnsEntity[]> {
        console.log(idBoard);
        return this.columnsService.getColumns(idBoard);
    }

    @Post('create')
    createColumns(
        @Param('idBoard') idBoard: number,
        @Body() dto: CreateColumnDto,
    ): Promise<ColumnsEntity> {
        console.log(dto);
        return this.columnsService.createColumn(dto, idBoard);
    }

    @Delete('delete')
    deleteColumns(
        @Param('idBoard') idBoard: number,
        @Body() dto: UpdateColumnDto,
    ): Promise<EnumInfoStatusesDto> {
        return this.columnsService.deleteColumn(dto, idBoard);
    }

    @Put('update')
    updateColumns(
        @Param('idBoard') idBoard: number,
        @Body() dto: UpdateColumnDto,
    ): Promise<ColumnsEntity> {
        return this.columnsService.updateColumn(dto, idBoard);
    }
}
