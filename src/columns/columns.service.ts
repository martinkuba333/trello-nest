import { Injectable } from '@nestjs/common';
import { ColumnsEntity } from './entities/columns.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateColumnDto, UpdateColumnDto } from '../dto/create-column.dto';
import { BoardEntity } from '../boards/entities/boards.entity';
import { EnumInfoStatusesDto } from '../dto/enum-info-statuses.dto';
import { NotFoundException } from '@nestjs/common';
import { ErrorsDto } from '../dto/errors.dto';

@Injectable()
export class ColumnsService {
    constructor(
        @InjectRepository(BoardEntity)
        private boardRepository: Repository<BoardEntity>,
        @InjectRepository(ColumnsEntity)
        private columnRepository: Repository<ColumnsEntity>,
    ) {}

    private async findBoard(idBoard: number): Promise<BoardEntity | null> {
        return await this.boardRepository.findOne({
            where: {
                id: idBoard,
            },
        });
    }

    async getColumns(idBoard: number): Promise<ColumnsEntity[]> {
        return await this.columnRepository
            .createQueryBuilder('columns')
            .where('columns.boardId = :idBoard', { idBoard })
            .getMany();
    }

    async createColumn(
        dto: CreateColumnDto,
        idBoard: number,
    ): Promise<ColumnsEntity> {
        const boardEntity = await this.findBoard(idBoard);
        if (boardEntity) {
            const columnEntity = new ColumnsEntity();
            columnEntity.tsCreation = Date.now();
            columnEntity.title = dto.title || '';
            columnEntity.board = boardEntity;
            return this.columnRepository.save(columnEntity);
        } else {
            throw new NotFoundException(ErrorsDto.WRONG_BOARD);
        }
    }

    async deleteColumn(
        dto: UpdateColumnDto,
        idBoard: number,
    ): Promise<EnumInfoStatusesDto> {
        const board = await this.findBoard(idBoard);
        if (!board) {
            throw new NotFoundException(ErrorsDto.WRONG_BOARD);
        }
        const columnEntity = await this.columnRepository.findOne({
            where: {
                id: dto.idColumn,
                board: board,
            },
        });

        if (columnEntity) {
            await this.columnRepository.delete(columnEntity);
            return EnumInfoStatusesDto.DELETED_BOARD;
        } else {
            throw new NotFoundException(ErrorsDto.COLUMN_NOT_FOUND);
        }
    }

    async updateColumn(
        dto: UpdateColumnDto,
        idBoard: number,
    ): Promise<ColumnsEntity> {
        if (!idBoard || !dto.idColumn) {
            throw new NotFoundException(ErrorsDto.WRONG_BOARD);
        }
        const boardEntity = await this.findBoard(idBoard);
        if (!boardEntity) {
            throw new NotFoundException(ErrorsDto.WRONG_BOARD);
        }
        const idColumn = dto.idColumn;
        const columnEntity = await this.columnRepository
            .createQueryBuilder('columns')
            .where('columns.id = :id', { id: idColumn })
            .andWhere('columns.boardId = :boardId', {
                boardId: boardEntity.id,
            })
            .getOne();

        if (!columnEntity) {
            throw new NotFoundException(ErrorsDto.WRONG_COLUMN);
        }
        columnEntity.title = dto.title;
        return await this.columnRepository.save(columnEntity);
    }
}
