import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BoardsModule } from './boards/boards.module';
import { ColumnsModule } from './columns/columns.module';
import { CardsModule } from './cards/cards.module';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            useFactory: async () => ({
                type: 'mysql',
                host: 'localhost',
                port: 3306,
                username: 'my_user',
                password: 'my_password',
                database: 'my_database',
                entities: ['dist/**/*.entity{.ts,.js}'],
                synchronize: true,
            }),
        }),
        BoardsModule,
        ColumnsModule,
        CardsModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
