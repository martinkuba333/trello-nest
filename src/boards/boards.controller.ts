import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
} from '@nestjs/common';
import { BoardsService } from './boards.service';
import { BoardEntity } from './entities/boards.entity';
import { CreateBoardDto, UpdateBoardDto } from '../dto/create-board.dto';
import { EnumInfoStatusesDto } from '../dto/enum-info-statuses.dto';

@Controller('boards')
export class BoardsController {
    constructor(private readonly boardsService: BoardsService) {}

    @Get()
    getBoards() {
        return this.boardsService.getBoards();
    }

    @Post('create')
    createBoard(@Body() dto: CreateBoardDto): Promise<BoardEntity> {
        return this.boardsService.createBoard(dto);
    }

    @Delete('delete')
    deleteBoard(@Body() dto: UpdateBoardDto): Promise<EnumInfoStatusesDto> {
        return this.boardsService.deleteBoard(dto);
    }

    @Put('update')
    updateBoard(@Body() dto: UpdateBoardDto): Promise<BoardEntity> {
        return this.boardsService.updateBoard(dto);
    }
}
