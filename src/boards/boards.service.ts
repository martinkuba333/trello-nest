import { Injectable, NotFoundException } from '@nestjs/common';
import { BoardEntity } from './entities/boards.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateBoardDto, UpdateBoardDto } from '../dto/create-board.dto';
import { EnumInfoStatusesDto } from '../dto/enum-info-statuses.dto';
import { ErrorsDto } from '../dto/errors.dto';

@Injectable()
export class BoardsService {
    constructor(
        @InjectRepository(BoardEntity)
        private boardRepository: Repository<BoardEntity>,
    ) {}

    async getBoards(): Promise<BoardEntity[]> {
        return await this.boardRepository.find();
    }

    async createBoard(dto: CreateBoardDto): Promise<BoardEntity> {
        const board = new BoardEntity();
        board.title = dto.title || '';
        board.tsCreation = Date.now();

        return await this.boardRepository.save(board);
    }

    async deleteBoard(dto: UpdateBoardDto): Promise<EnumInfoStatusesDto> {
        const board = await this.boardRepository.findOne({
            where: {
                id: dto.idBoard,
            },
        });

        if (!board) {
            throw new NotFoundException(ErrorsDto.BOARD_NOT_FOUND);
        }

        await this.boardRepository.remove(board);
        return EnumInfoStatusesDto.DELETED_BOARD;
    }

    async updateBoard(dto: UpdateBoardDto): Promise<BoardEntity> {
        const board = await this.boardRepository.findOne({
            where: {
                id: dto.idBoard,
            },
        });

        if (!board) {
            throw new NotFoundException(ErrorsDto.BOARD_NOT_FOUND);
        }

        board.title = dto.title;
        return await this.boardRepository.save(board);
    }
}
