import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('boards')
export class BoardEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 255 })
    title: string;

    @Column({ type: 'bigint' })
    tsCreation: number;
}
