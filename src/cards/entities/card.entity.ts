import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { BoardEntity } from '../../boards/entities/boards.entity';
import { ColumnsEntity } from '../../columns/entities/columns.entity';

@Entity('card')
export class CardEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 255 })
    title: string;

    @Column({ type: 'varchar', length: 255 })
    description: string;

    @Column({ type: 'bigint' })
    tsCreation: number;

    @ManyToOne(() => ColumnsEntity, columnsEntity => columnsEntity.id)
    column: ColumnsEntity;

    @Column({ type: 'integer' })
    cardOrder: number;
}
