import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';
import { CardEntity } from './entities/card.entity';
import { ColumnsEntity } from '../columns/entities/columns.entity';
import { BoardEntity } from '../boards/entities/boards.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([CardEntity, ColumnsEntity, BoardEntity]),
    ],
    providers: [CardsService],
    controllers: [CardsController],
})
export class CardsModule {}
