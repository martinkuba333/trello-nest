import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { EnumInfoStatusesDto } from '../dto/enum-info-statuses.dto';
import { CardEntity } from './entities/card.entity';
import { BoardEntity } from '../boards/entities/boards.entity';
import { ColumnsEntity } from '../columns/entities/columns.entity';
import { CreateCardDto, UpdateCardDto } from '../dto/create-card.dto';
import { NotFoundException } from '@nestjs/common';
import { ErrorsDto } from '../dto/errors.dto';

@Injectable()
export class CardsService {
    constructor(
        @InjectRepository(CardEntity)
        private cardRepository: Repository<CardEntity>,
        @InjectRepository(BoardEntity)
        private boardRepository: Repository<BoardEntity>,
        @InjectRepository(ColumnsEntity)
        private columnRepository: Repository<ColumnsEntity>,
    ) {}

    private async getColumn(
        idColumn: number,
        idBoard: number,
    ): Promise<ColumnsEntity | null> {
        const board = await this.findBoard(idBoard);
        if (!board) {
            throw new NotFoundException(ErrorsDto.WRONG_BOARD);
        }
        return await this.columnRepository.findOne({
            where: {
                id: idColumn,
                board: board,
            },
        });
    }

    private async findBoard(idBoard: number): Promise<BoardEntity | null> {
        return await this.boardRepository.findOne({
            where: {
                id: idBoard,
            },
        });
    }

    async getCards(idBoard: number, idColumn: number): Promise<CardEntity[]> {
        const column = await this.getColumn(idColumn, idBoard);
        if (!column) {
            throw new NotFoundException(ErrorsDto.WRONG_COLUMN);
        }

        return await this.cardRepository.find({
            where: {
                column: column,
            },
        });
    }

    async createCard(
        idBoard: number,
        idColumn: number,
        dto: CreateCardDto,
    ): Promise<CardEntity> {
        const column = await this.getColumn(idColumn, idBoard);
        if (!column) {
            throw new NotFoundException(ErrorsDto.WRONG_COLUMN);
        }
        const card = new CardEntity();

        card.column = column;
        card.title = dto.title || '';
        card.tsCreation = Date.now();
        card.description = dto.description || '';
        card.cardOrder = (await this.getHighestOrderNumber()) + 1;

        return await this.cardRepository.save(card);
    }

    private async getHighestOrderNumber(): Promise<number> {
        const result = await this.cardRepository
            .createQueryBuilder()
            .select('MAX(cardOrder)', 'maxCardOrder')
            .getRawOne();
        return result.maxCardOrder || 0;
    }

    async deleteCard(
        idBoard: number,
        idColumn: number,
        dto: UpdateCardDto,
    ): Promise<EnumInfoStatusesDto> {
        const column = await this.getColumn(idColumn, idBoard);
        if (!column) {
            throw new NotFoundException(ErrorsDto.WRONG_COLUMN);
        }

        const card = await this.cardRepository.findOne({
            where: {
                id: dto.idCard,
                column: column,
            },
        });

        if (!card) {
            throw new NotFoundException(ErrorsDto.CARD_NOT_FOUND);
        }

        await this.cardRepository.remove(card);
        return EnumInfoStatusesDto.DELETED_CARD;
    }

    async updateCard(
        idBoard: number,
        idColumn: number,
        dto: UpdateCardDto,
    ): Promise<CardEntity> {
        const column = await this.getColumn(idColumn, idBoard);
        if (!column) {
            throw new NotFoundException(ErrorsDto.WRONG_COLUMN);
        }
        let newColumn = null;
        if (dto.newIdColumn) {
            newColumn = await this.getColumn(dto.newIdColumn, idBoard);
            if (!newColumn) {
                throw new NotFoundException(ErrorsDto.WRONG_COLUMN);
            }
        }

        const card = await this.cardRepository.findOne({
            where: {
                id: dto.idCard,
                column: column,
            },
        });

        if (!card) {
            throw new NotFoundException(ErrorsDto.CARD_NOT_FOUND);
        }

        card.title = dto.title;
        card.description = dto.description;

        if (newColumn) {
            await this.changeOrderOfOtherCards(dto, card, newColumn, idColumn);
            card.column = newColumn;
        }

        if (dto.cardOrder && dto.cardOrder !== card.cardOrder) {
            card.cardOrder = dto.cardOrder;
        }
        return await this.cardRepository.save(card);
    }

    private async changeOrderOfOtherCards(
        dto: UpdateCardDto,
        card: CardEntity,
        newColumn: ColumnsEntity,
        idColumn: number
    ): Promise<void> {
        const searchedColumnId = newColumn ? newColumn.id : idColumn;
        const currentCardOrder = card.cardOrder;
        const newCardOrder = dto.cardOrder;

        const query = this.cardRepository
            .createQueryBuilder('card')
            .where('card.columnId=:searchedColumnId', { searchedColumnId });

        const increment = newCardOrder < currentCardOrder;
        if (increment) {
            query
                .andWhere('card.cardOrder>=:newCardOrder', { newCardOrder })
                .andWhere('card.cardOrder<:currentCardOrder', {
                    currentCardOrder,
                });
        } else {
            query
                .andWhere('card.cardOrder<=:newCardOrder', { newCardOrder })
                .andWhere('card.cardOrder>:currentCardOrder', {
                    currentCardOrder,
                });
        }

        const cards = await query.getMany();
        for (const cardEntity of cards) {
            increment ? cardEntity.cardOrder++ : cardEntity.cardOrder--;
            await this.cardRepository.save(cardEntity);
        }
    }
}
