import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
} from '@nestjs/common';
import { EnumInfoStatusesDto } from '../dto/enum-info-statuses.dto';
import { CardsService } from './cards.service';
import { CardEntity } from './entities/card.entity';
import { CreateCardDto, UpdateCardDto } from '../dto/create-card.dto';

//todo guard if board exists
//todo guard if column exists
@Controller('boards/:idBoard/column/:idColumn/card')
export class CardsController {
    constructor(private readonly cardsService: CardsService) {}

    @Get('get')
    getCards(
        @Param('idBoard') idBoard: number,
        @Param('idColumn') idColumn: number,
    ): Promise<CardEntity[]> {
        return this.cardsService.getCards(idBoard, idColumn);
    }

    @Post('create')
    createCard(
        @Param('idBoard') idBoard: number,
        @Param('idColumn') idColumn: number,
        @Body() dto: CreateCardDto,
    ): Promise<CardEntity> {
        return this.cardsService.createCard(idBoard, idColumn, dto);
    }

    @Delete('delete')
    deleteCard(
        @Param('idBoard') idBoard: number,
        @Param('idColumn') idColumn: number,
        @Body() dto: UpdateCardDto,
    ): Promise<EnumInfoStatusesDto> {
        return this.cardsService.deleteCard(idBoard, idColumn, dto);
    }

    @Put('update')
    updateCard(
        @Param('idBoard') idBoard: number,
        @Param('idColumn') idColumn: number,
        @Body() dto: UpdateCardDto,
    ): Promise<CardEntity> {
        return this.cardsService.updateCard(idBoard, idColumn, dto);
    }
}
